create database movies3;
use movies3;
create table moviedataset(id int(10)primary key not null,title varchar(45),year int(10),storyline varchar(105),imdbRating int(10),Classification varchar(45));

desc moviedataset;


iNSERT INTO moviedataset VALUES (1,'Game NIGHT',2018,'"A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery.',7,'coming soon');

iNSERT INTO moviedataset VALUES (2,'Area X: Annihilation',2018,' "A biologist an anthropologist, a psychologist, a surveyor, and a linguist.",',9,'Movies in Theaters');

iNSERT INTO moviedataset VALUES (3,'Hannah',2017,' A group of friends who meet regularly for game nights find themselves trying to solve a murder mystery',8,'top rated indian');

iNSERT INTO moviedataset VALUES (4,'The Lodgers',2017,'1920, rural Ireland. Anglo Irish twins Rachel and Edward share a strange existence in their crumbli',7,'coming soon');

iNSERT INTO moviedataset VALUES (5,'Beast of Burden',2018,'Sean Haggerty only has an hour to deliver his illegal cargo. An hour to reassure a drug cartel, a hitman,',8,'coming soon');

iNSERT INTO moviedataset VALUES (21,'Thanhaji',2017,'The Unsung Warrior is a 2020 Indian Hindi-language historical action film,directed by Om Raut',9,'Movies in Theaters');

iNSERT INTO moviedataset VALUES (22,'Panipat',2017,'Najib receives intelligence that the Marathas have retreated,they are marching north to Delhi',9,'top rated movies');

iNSERT INTO moviedataset VALUES (23,'Saaho',2017,'Ibrahim reveals that Vishwank was not Roy son, but Ibrahim own son; his real name is Iqbal',7,'top rated indian');

iNSERT INTO moviedataset VALUES (24,'Kabir Singh',2019,'Kabir and his friends announce that he has exclusively claimed Preeti',7,'Movies in Theaters');

iNSERT INTO moviedataset VALUES (25,'Red lion',2019,'Khan had interviewed the Phogat sisters on his talk show Satyamev Jayate',9,'top rated indian');


select *from moviedataset;
package com.test;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;
import com.factory.CategoryMovies;
import com.factory.Classification;
import model.Movie;
public class main {
public static void main(String[] args) {
		int choice;
        Scanner sc = new Scanner(System.in);
		do
		{
          System.out.println("WELCOME");
			System.out.println("Please select a category");
			System.out.println("1)Coming Soon movies\n"
							   + "2)Movies In Theaters\n"
							   + "3)Top Rated Indian\n"
							   + "4)Top Rated Movies\n"
							   + "5) to exit");
			System.out.println("Enter your choice");
			System.out.println();
            choice = sc.nextInt();
            switch(choice) {
			case 1:
				Classification cs=CategoryMovies.setMovieType(1);
				try {
					List<Movie> movie=cs.movieType();
					System.out.println("Coming Soon Movies");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 2:
				Classification mit=CategoryMovies.setMovieType(2);
				try {
					List<Movie> movie=mit.movieType();
					System.out.println("Movies in Theaters");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 3:
				Classification tri=CategoryMovies.setMovieType(3);
				try {
					List<Movie> movie=tri.movieType();
					System.out.println("Top Rated Indian");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
			case 4:
				Classification trm=CategoryMovies.setMovieType(4);
				try {
					List<Movie> movie=trm.movieType();
					System.out.println(" Top Rated Movies");
					for(Movie mo:movie) {
						System.out.println(mo+ "\n");
					}
				}catch(SQLException e) {
					e.printStackTrace();
				}
				break;
             case 5:
				System.exit(0);
				break;
			default:
				System.out.println("Invalid Choice");
				break;
			}
		}while(choice!=5);
	}
}


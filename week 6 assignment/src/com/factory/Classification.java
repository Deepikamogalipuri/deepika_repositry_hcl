package com.factory;
import java.sql.SQLException;
import java.util.List;
import model.Movie;
public interface Classification {
	public List<Movie> movieType() throws SQLException;
}

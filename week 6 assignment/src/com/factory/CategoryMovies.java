package com.factory;
public interface CategoryMovies {
 public static Classification setMovieType(int type) {
            if(type==1) {
				return new ComingMovies();
			}
			else if(type==2) {
				return new MovieInTheater();
			}
			else if(type==3) {
				return new TopRatedindianmovies();
			}
			else if(type==4) {
				return  new TopRatedMovies();
			}
			else {
				return null;
			}
		}
}

package com.factory;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.sql.Statement;
import java.util.ArrayList;
import database.DBConnection;
import model.Movie;
public class ComingMovies implements Classification {
	Connection con =DBConnection.getInstance().getConnection();
    public ComingMovies() {
    }
	@Override
	public List<Movie> movieType() throws SQLException {
     //To display movies
		List<Movie> movies=new ArrayList<Movie>();
		//Query to get data 
		String sql="select id,title,year,storyline,imdbRating,classification from moviedataset where Classification ='coming soon'";
		Statement statement =con.createStatement();
		ResultSet rs = statement.executeQuery(sql);
		while(rs.next())
		{
			Movie movie = new Movie();
			movie.setId(rs.getInt(1));
			movie.setTitle(rs.getString(2));
			movie.setYear(rs.getInt(3));
			movie.setStoryline(rs.getString(4));
			movie.setImdbRating(rs.getInt(5));
			movie.setClassification(rs.getString(6));
			movies.add(movie);
		}
      return movies;	
	}
}

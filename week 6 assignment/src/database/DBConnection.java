package database;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class DBConnection {
	private static DBConnection instance;
	private static Connection con;
	private DBConnection() {
   }
	public static DBConnection getInstance() {
		if(instance == null) {
			instance = new DBConnection();
		}
		return instance;
	}
	public Connection getConnection() {	
		if(con == null) {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection(
					 "jdbc:mysql://localhost:3306/movies3","root","Deepika@33");
				System.out.println("driver loaded and connected");
			} catch (ClassNotFoundException e) {
				System.out.println("error driver not loaded");
				e.printStackTrace();
			} catch (SQLException e) {
				System.out.println("cannot connect to database");
				e.printStackTrace();
			}
		}
		return con;	
	}
}
